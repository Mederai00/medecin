<?php

namespace App\Controller;

use App\Entity\Medecin;
use App\Entity\Medicament;
use App\Repository\MedecinRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/medecin")
 */
class MedecinController extends AbstractController
{
    private $manager;
    private $medecinRepository;
    private $serializer;

    public function __construct(MedecinRepository $medecinRepository, SerializerInterface $serializer, EntityManagerInterface $manager)
    {
        $this->manager = $manager;
        $this->medecinRepository = $medecinRepository;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/", name="get_medecin")
     */
    public function index()
    {
        return
            new JsonResponse(
                $this->serializer->serialize(
                    $this->medecinRepository->findAll_ToJson(),
                    'json'
                )
            );
    }

    /**
     * @Route("/Modifier/{id}", name="edit_medecin", methods={"POST"})
     */
    public function edit(Medecin $medecin, Request $request)
    {
    }

    /**
     * @Route("/Ajouter", name="add_medecin")
     */
    public function add(Request $request)
    {
    }

    /**
     * @Route("/Supprimer/{id}", name="delete_medecin")
     */
    public function delete(Medecin $medecin)
    {
        $this->manager->remove($medecin);
        $this->manager->flush();
        return new JsonResponse("Supprimé avec succèes.");
    }
}
